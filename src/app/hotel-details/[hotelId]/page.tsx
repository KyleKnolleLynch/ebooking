import HotelDetailsClient from '@/components/hotel/HotelDetailsClient'
import { getHotelById } from '../../../../actions/getHotelById'

interface HotelDetailsProps {
  params: {
    hotelId: string
  }
}

export default async function HotelDetails({ params }: HotelDetailsProps) {
  const hotel = await getHotelById(params.hotelId)
  if (!hotel) return <div className='text-lg'>Oops! That hotel cannot be found.</div>
  return (
    <div>
        <HotelDetailsClient hotel={hotel} />
    </div>
  )
}
