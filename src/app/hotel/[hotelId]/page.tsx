import AddHotelForm from '@/components/hotel/AddHotelForm'
import { getHotelById } from '../../../../actions/getHotelById'
import { auth } from '@clerk/nextjs'

interface HotelPageProps {
  params: {
    hotelId: string
  }
}

export default async function Hotel({ params }: HotelPageProps) {
  const hotel = await getHotelById(params.hotelId)
  const { userId } = auth()

  if (!userId)
    return (
      <p className='text-lg font-bold text-red-500'>Not authenticated...</p>
    )

  if (hotel && hotel.userId !== userId)
    return <p className='text-lg font-bold text-red-500'>Access denied</p>
  return (
    <section>
      <AddHotelForm hotel={hotel} />
    </section>
  )
}
