import type { Metadata } from 'next'
import { ClerkProvider } from '@clerk/nextjs'

import { ThemeProvider } from '@/components/providers/ThemeProvider'
import Navbar from '@/components/layout/Navbar'
import '../styles/globals.css'
import Container from '@/components/layout/Container'
import { Toaster } from '@/components/ui/toaster'

export const metadata: Metadata = {
  title: 'Ebooking',
  description: 'Mock e-booking app using Next.js',
  // icons: { icon: 'logo.svg' },
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <ClerkProvider>
      <html lang='en' suppressHydrationWarning>
        <body className='font-sans'>
          <ThemeProvider
            attribute='class'
            defaultTheme='system'
            enableSystem
            disableTransitionOnChange
          >
            <div className='flex min-h-screen flex-col bg-secondary'>
              <Navbar />
              <main className='flex-grow'>
                <Container>{children}</Container>
              </main>
            </div>
            <Toaster />
          </ThemeProvider>
        </body>
      </html>
    </ClerkProvider>
  )
}
