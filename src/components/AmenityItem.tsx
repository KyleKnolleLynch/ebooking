export default function AmenityItem({
  children,
}: {
  children: React.ReactNode
}) {
  return <li className='flex items-center gap-2'>{children}</li>
}
