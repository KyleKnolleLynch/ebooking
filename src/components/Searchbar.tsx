'use client'

import { Search } from 'lucide-react'
import { Input } from './ui/input'

export default function Searchbar() {
  return (
    <div className='relative hidden max-w-lg sm:block lg:w-full'>
      <Search className='absolute bottom-0 left-4 top-0 my-auto size-4 text-muted-foreground' />
      <Input placeholder='Search' className='bg-primary-foreground px-10' />
    </div>
  )
}
