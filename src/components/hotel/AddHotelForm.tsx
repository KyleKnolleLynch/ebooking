'use client'

import { useEffect, useState } from 'react'
import { useRouter } from 'next/navigation'
import Image from 'next/image'
import * as z from 'zod'
import { zodResolver } from '@hookform/resolvers/zod'
import { useForm } from 'react-hook-form'
import axios from 'axios'
import { ICity, IState } from 'country-state-city'
import { Hotel, Room } from '@prisma/client'
import {
  Eye,
  Loader2,
  PencilIcon,
  PencilLine,
  PlusCircle,
  Terminal,
  Trash,
  XCircle,
} from 'lucide-react'

import useLocation from '@/hooks/useLocation'
import { UploadButton } from '@/utils/uploadthing'
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form'
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select'

import { Input } from '../ui/input'
import { Textarea } from '../ui/textarea'
import { Checkbox } from '../ui/checkbox'
import { Button } from '../ui/button'
import { useToast } from '../ui/use-toast'
import { Alert, AlertDescription, AlertTitle } from '../ui/alert'
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '../ui/dialog'
import { Separator } from '../ui/separator'
import AddRoomForm from '@/components/room/AddRoomForm'
import RoomCard from '@/components/room/RoomCard'

interface AddHotelFormProps {
  hotel: HotelWithRooms | null
}

export type HotelWithRooms = Hotel & {
  rooms: Room[]
}

const formSchema = z.object({
  title: z.string().min(3, {
    message: 'Title must be at least 3 characters long',
  }),
  description: z.string().min(10, {
    message: 'Title must be at least 10 characters long',
  }),
  image: z.string().min(1, {
    message: 'Image is required',
  }),
  country: z.string().min(1, {
    message: 'Country is required',
  }),
  state: z.string().optional(),
  city: z.string().optional(),
  locationDescription: z.string().min(10, {
    message: 'Description must be at least 10 characters long',
  }),
  gym: z.boolean().optional(),
  spa: z.boolean().optional(),
  bar: z.boolean().optional(),
  laundry: z.boolean().optional(),
  freeBreakfast: z.boolean().optional(),
  coffeeBar: z.boolean().optional(),
  freeWifi: z.boolean().optional(),
  freeShuttle: z.boolean().optional(),
  bikeRental: z.boolean().optional(),
  pool: z.boolean().optional(),
})

export default function AddHotelForm({ hotel }: AddHotelFormProps) {
  const [image, setImage] = useState<string | undefined>(hotel?.image)
  const [imageIsDeleting, setImageIsDeleting] = useState(false)
  const [states, setStates] = useState<IState[]>([])
  const [cities, setCities] = useState<ICity[]>([])
  const [isLoading, setIsLoading] = useState(false)
  const [isHotelDeleting, setIsHotelDeleting] = useState<boolean>(false)
  const [open, setOpen] = useState<boolean>(false)

  const { toast } = useToast()
  const router = useRouter()
  const { getAllCountries, getCountryStates, getStateCities } = useLocation()
  const countries = getAllCountries()

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: hotel || {
      title: '',
      description: '',
      image: '',
      country: '',
      state: '',
      city: '',
      locationDescription: '',
      gym: false,
      spa: false,
      bar: false,
      laundry: false,
      freeBreakfast: false,
      coffeeBar: false,
      freeWifi: false,
      freeShuttle: false,
      bikeRental: false,
      pool: false,
    },
  })

  useEffect(() => {
    if (typeof image === 'string') {
      form.setValue('image', image, {
        shouldValidate: true,
        shouldDirty: true,
        shouldTouch: true,
      })
    }
  }, [form, image])

  useEffect(() => {
    const selectedCountry = form.watch('country')
    const countryStates = getCountryStates(selectedCountry)
    if (countryStates) setStates(countryStates)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form.watch('country')])

  useEffect(() => {
    const selectedCountry = form.watch('country')
    const selectedState = form.watch('state')
    const stateCities = getStateCities(selectedCountry, selectedState)
    if (stateCities) setCities(stateCities)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form.watch('country'), form.watch('state')])

  function onSubmit(values: z.infer<typeof formSchema>) {
    setIsLoading(true)
    if (hotel) {
      axios
        .patch(`/api/hotel/${hotel.id}`, values)
        .then(res => {
          toast({
            variant: 'success',
            description: '🚀 Hotel updated!',
          })
          setIsLoading(false)
          router.push(`/hotel/${res.data.id}`)
        })
        .catch(error => {
          console.log(error)
          toast({
            variant: 'destructive',
            description: 'Something went wrong!',
          })
          setIsLoading(false)
        })
    } else {
      axios
        .post('/api/hotel', values)
        .then(res => {
          toast({
            variant: 'success',
            description: '🎆 Hotel created!',
          })
          setIsLoading(false)
          router.push(`/hotel/${res.data.id}`)
        })
        .catch(error => {
          console.log(error)
          toast({
            variant: 'destructive',
            description: 'Something went wrong!',
          })
          setIsLoading(false)
        })
    }
  }

  async function handleImageDelete(image: string) {
    setImageIsDeleting(true)
    const imageKey = image.substring(image.lastIndexOf('/') + 1)

    try {
      const res = await axios.post('/api/uploadthing/delete', { imageKey })
      if (res.data.success) {
        setImage('')
        toast({
          variant: 'success',
          description: 'Image removed!',
        })
      }
    } catch (error) {
      toast({
        variant: 'destructive',
        description: 'Something went wrong',
      })
    } finally {
      setImageIsDeleting(false)
    }
  }

  async function handleDeleteHotel(hotel: HotelWithRooms) {
    setIsHotelDeleting(true)

    try {
      await axios.delete(`/api/hotel/${hotel.id}`)

      setIsHotelDeleting(false)
      toast({
        variant: 'success',
        description: 'Hotel deleted!',
      })
      router.push('/hotel/new')
    } catch (error: any) {
      setIsHotelDeleting(false)
      toast({
        variant: 'destructive',
        description: `Hotel deletion was unsuccessful! - ${error.message}`,
      })
    }
  }

  const handleDialogOpen = (): void => {
    setOpen(prev => !prev)
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className='space-y-6'>
        <h2 className='text-lg font-bold'>
          {hotel ? 'Update your hotel' : 'Describe your hotel'}
        </h2>
        <div className='grid grid-cols-1 gap-6 md:grid-cols-2'>
          <div className='flex flex-col gap-6'>
            <FormField
              control={form.control}
              name='title'
              render={({ field }) => (
                <FormItem>
                  <FormLabel>
                    Hotel Title
                    <span className='text-2xl font-bold leading-none'>*</span>
                  </FormLabel>
                  <FormDescription>
                    Please provide a hotel name.
                  </FormDescription>
                  <FormControl>
                    <Input
                      placeholder='Beach Hotel'
                      {...field}
                      className='dark:border dark:border-muted-foreground'
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name='description'
              render={({ field }) => (
                <FormItem>
                  <FormLabel>
                    Hotel Description
                    <span className='text-2xl font-bold leading-none'>*</span>
                  </FormLabel>
                  <FormDescription>
                    Describe your desired hotel.
                  </FormDescription>
                  <FormControl>
                    <Textarea
                      placeholder='Beach hotel with great views and fun amenities'
                      className='dark:border dark:border-muted-foreground'
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <div>
              <FormLabel>Choose Amenities</FormLabel>
              <FormDescription>Select amenities you enjoy.</FormDescription>
              <div className='mt-2 grid grid-cols-2 gap-4'>
                <FormField
                  control={form.control}
                  name='gym'
                  render={({ field }) => (
                    <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                      <FormControl>
                        <Checkbox
                          id='gym'
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </FormControl>
                      <FormLabel htmlFor='gym'>Gym</FormLabel>

                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='pool'
                  render={({ field }) => (
                    <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                      <FormControl>
                        <Checkbox
                          id='pool'
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </FormControl>
                      <FormLabel htmlFor='pool'>Pool</FormLabel>

                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='bar'
                  render={({ field }) => (
                    <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                      <FormControl>
                        <Checkbox
                          id='bar'
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </FormControl>
                      <FormLabel htmlFor='bar'>Bar</FormLabel>

                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='freeBreakfast'
                  render={({ field }) => (
                    <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                      <FormControl>
                        <Checkbox
                          id='freeBreakfast'
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </FormControl>
                      <FormLabel htmlFor='freeBreakfast'>
                        Free breakfast
                      </FormLabel>

                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='coffeeBar'
                  render={({ field }) => (
                    <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                      <FormControl>
                        <Checkbox
                          id='coffeeBar'
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </FormControl>
                      <FormLabel htmlFor='coffeeBar'>Coffee bar</FormLabel>

                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='laundry'
                  render={({ field }) => (
                    <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                      <FormControl>
                        <Checkbox
                          id='laundry'
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </FormControl>
                      <FormLabel htmlFor='laundry'>Laundry room</FormLabel>

                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='freeWifi'
                  render={({ field }) => (
                    <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                      <FormControl>
                        <Checkbox
                          id='freeWifi'
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </FormControl>
                      <FormLabel htmlFor='freeWifi'>Free wifi</FormLabel>

                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='bikeRental'
                  render={({ field }) => (
                    <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                      <FormControl>
                        <Checkbox
                          id='bikeRental'
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </FormControl>
                      <FormLabel htmlFor='bikeRental'>Bike rental</FormLabel>

                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='freeShuttle'
                  render={({ field }) => (
                    <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                      <FormControl>
                        <Checkbox
                          id='freeShuttle'
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </FormControl>
                      <FormLabel htmlFor='freeShuttle'>Free shuttle</FormLabel>

                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='spa'
                  render={({ field }) => (
                    <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                      <FormControl>
                        <Checkbox
                          id='spa'
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </FormControl>
                      <FormLabel htmlFor='spa'>Spa</FormLabel>

                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <FormField
              control={form.control}
              name='image'
              render={({ field }) => (
                <FormItem className='my-2 flex flex-col space-y-3'>
                  <FormLabel>
                    Upload an image
                    <span className='text-2xl font-bold leading-none'>*</span>
                  </FormLabel>
                  <FormDescription>
                    Choose an image that will showcase your hotel
                  </FormDescription>
                  <FormControl>
                    {image ? (
                      <>
                        <div className='relative mt-4 max-h-[400px] min-h-[200px] min-w-[200px] max-w-[400px]'>
                          <Image
                            fill
                            sizes='(max-width: 768px) 200px, 400px'
                            src={image}
                            alt='Hotel image'
                            className='object-contain'
                          />
                          <Button
                            type='button'
                            aria-label='Delete hotel image'
                            size='icon'
                            variant='ghost'
                            className='absolute -right-3 top-0'
                            onClick={() => handleImageDelete(image)}
                          >
                            {imageIsDeleting ? (
                              <Loader2 className='animate-spin' />
                            ) : (
                              <XCircle />
                            )}
                          </Button>
                        </div>
                      </>
                    ) : (
                      <>
                        <div className='flex max-w-[400px] flex-col items-center rounded-md border-2 border-dashed p-10 dark:border-muted-foreground/50'>
                          <UploadButton
                            endpoint='imageUploader'
                            onClientUploadComplete={res => {
                              setImage(res[0].url)
                              toast({
                                variant: 'success',
                                description: 'Upload successful!',
                              })
                              console.log('Files: ', res)
                            }}
                            onUploadError={(error: Error) => {
                              toast({
                                variant: 'destructive',
                                description: `Upload failed! ${error.message}`,
                              })
                            }}
                          />
                        </div>
                      </>
                    )}
                  </FormControl>
                </FormItem>
              )}
            />
          </div>
          <div className='flex flex-col gap-6'>
            <div className='grid grid-cols-1 gap-6 md:grid-cols-2'>
              <FormField
                control={form.control}
                name='country'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>
                      Select Country
                      <span className='text-2xl font-bold leading-none'>*</span>
                    </FormLabel>
                    <FormDescription>
                      In which country is your property located?
                    </FormDescription>
                    <Select
                      disabled={isLoading}
                      onValueChange={field.onChange}
                      value={field.value}
                      defaultValue={field.value}
                    >
                      <SelectTrigger className='bg-background'>
                        <SelectValue
                          placeholder='Select a country'
                          defaultValue={field.value}
                        />
                      </SelectTrigger>
                      <SelectContent>
                        {countries.map(country => (
                          <SelectItem
                            key={country.isoCode}
                            value={country.isoCode}
                          >
                            {country.name}
                          </SelectItem>
                        ))}
                      </SelectContent>
                    </Select>
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='state'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Select State</FormLabel>
                    <FormDescription>
                      In which state is your property located?
                    </FormDescription>
                    <Select
                      disabled={isLoading || !states.length}
                      onValueChange={field.onChange}
                      value={field.value}
                      defaultValue={field.value}
                    >
                      <SelectTrigger className='bg-background'>
                        <SelectValue
                          placeholder='Select a state'
                          defaultValue={field.value}
                        />
                      </SelectTrigger>
                      <SelectContent>
                        {states.map(state => (
                          <SelectItem key={state.isoCode} value={state.isoCode}>
                            {state.name}
                          </SelectItem>
                        ))}
                      </SelectContent>
                    </Select>
                  </FormItem>
                )}
              />
            </div>
            <FormField
              control={form.control}
              name='city'
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Select City</FormLabel>
                  <FormDescription>
                    In which city/town is your property located?
                  </FormDescription>
                  <Select
                    disabled={isLoading || !cities.length}
                    onValueChange={field.onChange}
                    value={field.value}
                    defaultValue={field.value}
                  >
                    <SelectTrigger className='bg-background'>
                      <SelectValue
                        placeholder='Select a city'
                        defaultValue={field.value}
                      />
                    </SelectTrigger>
                    <SelectContent>
                      {cities.map(city => (
                        <SelectItem key={city.name} value={city.name}>
                          {city.name}
                        </SelectItem>
                      ))}
                    </SelectContent>
                  </Select>
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name='locationDescription'
              render={({ field }) => (
                <FormItem>
                  <FormLabel>
                    Location Description
                    <span className='text-2xl font-bold leading-none'>*</span>
                  </FormLabel>
                  <FormDescription>
                    Provide a location description of your desired hotel.
                  </FormDescription>
                  <FormControl>
                    <Textarea
                      placeholder='Located at the base of the mountain by the coastline'
                      className='dark:border dark:border-muted-foreground'
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            {hotel && !hotel.rooms.length && (
              <Alert className='bg-amber-800 text-white'>
                <Terminal className='size-4 stroke-white' />
                <AlertTitle>One last step!</AlertTitle>
                <AlertDescription>
                  Your hotel was created successfully 🎉
                  <p>Please add some rooms to complete your hotel setup.</p>
                </AlertDescription>
              </Alert>
            )}
            <div className='flex flex-wrap justify-between gap-2'>
              {/* Update/Create button */}
              {hotel ? (
                <Button disabled={isLoading} className='max-w-36'>
                  {isLoading ? (
                    <>
                      <Loader2 className='mr-2 size-4 animate-spin' />
                      Updating
                    </>
                  ) : (
                    <>
                      <PencilLine className='mr-2 size-4' />
                      Update
                    </>
                  )}
                </Button>
              ) : (
                <Button className='max-w-36'>
                  {isLoading ? (
                    <>
                      <Loader2 className='mr-2 size-4 animate-spin' />
                      Creating
                    </>
                  ) : (
                    <>
                      <PencilIcon className='mr-2 size-4' />
                      Create hotel
                    </>
                  )}
                </Button>
              )}

              {/* View button */}
              {hotel && (
                <Button
                  type='button'
                  variant='outline'
                  className='max-w-36'
                  onClick={() => router.push(`/hotel-details/${hotel.id}`)}
                >
                  <Eye className='mr-2 size-4' />
                  View
                </Button>
              )}

              {/* Modal/Dialog button */}
              {hotel && (
                <Dialog open={open} onOpenChange={setOpen}>
                  <DialogTrigger>
                    <Button
                      type='button'
                      variant='outline'
                      className='max-w-36'
                    >
                      <PlusCircle className='mr-2 size-4' />
                      Add room
                    </Button>
                  </DialogTrigger>
                  <DialogContent className='w-11/12 max-w-[900px]'>
                    <DialogHeader>
                      <DialogTitle>Add a room</DialogTitle>
                      <DialogDescription>
                        Add details for your hotel room of choice.
                      </DialogDescription>
                    </DialogHeader>
                    <AddRoomForm
                      hotel={hotel}
                      handleDialogOpen={handleDialogOpen}
                    />
                  </DialogContent>
                </Dialog>
              )}

              {/* Delete button */}
              {hotel && (
                <Button
                  type='button'
                  disabled={isHotelDeleting || isLoading}
                  onClick={() => handleDeleteHotel(hotel)}
                  variant='destructive'
                  className='max-w-36'
                >
                  {isHotelDeleting ? (
                    <>
                      <Loader2 className='mr-2 size-4 animate-spin' />
                      Deleting
                    </>
                  ) : (
                    <>
                      <Trash className='mr-2 size-4' />
                      Delete
                    </>
                  )}
                </Button>
              )}
            </div>
            {hotel && hotel.rooms.length && (
              <div>
                <Separator className='bg-foreground/30' />
                <h3 className='my-4 text-lg font-bold'>Hotel Rooms</h3>
                <div className='grid grid-cols-1 gap-6 2xl:grid-cols-2'>
                  {hotel.rooms.map(room => (
                    <RoomCard key={room.id} hotel={hotel} room={room} />
                  ))}
                </div>
              </div>
            )}
          </div>
        </div>
      </form>
    </Form>
  )
}
