'use client'

import Image from 'next/image'
import { usePathname, useRouter } from 'next/navigation'
import { TbSwimming } from 'react-icons/tb'
import { Croissant, MapPin } from 'lucide-react'

import { HotelWithRooms } from './AddHotelForm'
import AmenityItem from '../AmenityItem'
import { cn } from '@/lib/utils'
import useLocation from '@/hooks/useLocation'
import { Button } from '../ui/button'

export default function HotelCard({ hotel }: { hotel: HotelWithRooms }) {
  const pathname = usePathname()
  const router = useRouter()
  const areMyHotels = pathname.includes('my-hotels')

  const { getCountryByCode } = useLocation()
  const country = getCountryByCode(hotel.country)

  return (
    <div
      onClick={() => !areMyHotels && router.push(`/hotel-details/${hotel.id}`)}
      className={cn(
        'col-span-1 cursor-pointer transition hover:scale-105',
        areMyHotels && 'cursor-default',
      )}
    >
      <div className='flex flex-col gap-2 rounded-lg border border-primary/10 bg-background/50 sm:flex-row'>
        <div className='relative aspect-square h-52 w-full flex-1 overflow-hidden rounded-s-lg'>
          <Image
            fill
            src={hotel.image}
            alt={hotel.title}
            className='h-full w-full object-cover'
          />
        </div>
        <div className='flex h-52 flex-1 flex-col justify-between gap-1 px-1 py-2 text-sm'>
          <h3 className='text-xl font-bold'>{hotel.title}</h3>
          <p className='line-clamp-2'>{hotel.description}</p>
          <ul>
            <AmenityItem>
              <MapPin className='size-4' />
              {hotel.city}, {country?.name}
            </AmenityItem>
            {hotel.pool && (
              <AmenityItem>
                <TbSwimming className='size-4' />
                Pool
              </AmenityItem>
            )}
            {hotel.freeBreakfast && (
              <AmenityItem>
                <Croissant className='size-4' />
                Free Breakfast
              </AmenityItem>
            )}
          </ul>
          <div className='flex items-center justify-between'>
            <div className='flex items-center gap-1'>
              {hotel?.rooms[0]?.roomPrice && (
                <>
                  <p className='font-bold'>${hotel?.rooms[0]?.roomPrice}</p>
                  <p className='text-xs'>/ 24 hours</p>
                </>
              )}
            </div>
            {areMyHotels && (
              <Button
                variant='outline'
                onClick={() => router.push(`/hotel${hotel.id}`)}
              >
                Edit
              </Button>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}
