'use client'

import Image from 'next/image'
import { Booking } from '@prisma/client'
import {
  Bike,
  Bus,
  Coffee,
  Croissant,
  Dumbbell,
  MapPin,
  Martini,
  Wifi,
} from 'lucide-react'
import { TbSwimming } from 'react-icons/tb'
import { FaSpa } from 'react-icons/fa6'
import { MdOutlineLocalLaundryService } from 'react-icons/md'

import useLocation from '@/hooks/useLocation'
import { HotelWithRooms } from './AddHotelForm'
import AmenityItem from '../AmenityItem'
import RoomCard from '../room/RoomCard'

export default function HotelDetailsClient({
  hotel,
  bookings,
}: {
  hotel: HotelWithRooms
  bookings?: Booking[]
}) {
  const { getCountryByCode, getStateByCode } = useLocation()
  const country = getCountryByCode(hotel.country)
  const state = getStateByCode(hotel.country, hotel.state)
  return (
    <div className='flex flex-col gap-6 pb-2'>
      <div className='relative aspect-square h-[200px] w-full overflow-hidden rounded-lg md:h-[400px]'>
        <Image
          fill
          src={hotel.image}
          alt={hotel.title}
          className='object-cover'
        />
      </div>
      <div>
        <h2 className='text-xl font-bold md:text-3xl'>{hotel.title}</h2>
        <ul className='mt-4'>
          <AmenityItem>
            <MapPin className='size-4' />
            {hotel.city}, {state?.name}, {country?.name}
          </AmenityItem>
        </ul>
        <h3 className='mt-4 text-lg font-bold md:text-xl'>Location Details</h3>
        <p className='mt-2'>{hotel.locationDescription}</p>
        <h3 className='mt-4 text-lg font-bold md:text-xl'>About this hotel</h3>
        <p className='mt-2'>{hotel.description}</p>
        <h3 className='mt-4 text-lg font-bold md:text-xl'>Feature Amenities</h3>
        <ul className='grid grid-cols-2 content-start gap-4 text-sm md:grid-cols-3'>
          {hotel.pool && (
            <AmenityItem>
              <TbSwimming className='size-6' />
              Pool
            </AmenityItem>
          )}
          {hotel.freeBreakfast && (
            <AmenityItem>
              <Croissant className='size-6' />
              Free breakfast
            </AmenityItem>
          )}
          {hotel.freeWifi && (
            <AmenityItem>
              <Wifi className='size-6' />
              Free Wifi
            </AmenityItem>
          )}
          {hotel.gym && (
            <AmenityItem>
              <Dumbbell className='size-6' />
              Gym
            </AmenityItem>
          )}
          {hotel.bar && (
            <AmenityItem>
              <Martini className='size-6' />
              Bar
            </AmenityItem>
          )}
          {hotel.spa && (
            <AmenityItem>
              <FaSpa className='size-6' />
              Spa
            </AmenityItem>
          )}
          {hotel.laundry && (
            <AmenityItem>
              <MdOutlineLocalLaundryService className='size-6' />
              Laundry Room
            </AmenityItem>
          )}
          {hotel.coffeeBar && (
            <AmenityItem>
              <Coffee className='size-6' />
              Coffee bar
            </AmenityItem>
          )}
          {hotel.freeShuttle && (
            <AmenityItem>
              <Bus className='size-6' />
              Free shuttle
            </AmenityItem>
          )}
          {hotel.bikeRental && (
            <AmenityItem>
              <Bike className='size-6' />
              Bicycle rental
            </AmenityItem>
          )}
        </ul>
      </div>
      <section>
        {!!hotel.rooms.length && (
          <div>
            <h3 className='my-4 text-lg font-bold'>Hotel Rooms</h3>
            <div className='grid grid-cols-1 gap-6 md:grid-cols-2 xl:grid-cols-3'>
              {hotel.rooms.map(room => (
                <RoomCard
                  key={room.id}
                  hotel={hotel}
                  room={room}
                  bookings={bookings}
                />
              ))}
            </div>
          </div>
        )}
      </section>
    </div>
  )
}
