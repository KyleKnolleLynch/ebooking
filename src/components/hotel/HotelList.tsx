import { HotelWithRooms } from "./AddHotelForm";
import HotelCard from "./HotelCard";

export default function HotelList({ hotels}: {hotels: HotelWithRooms[]}) {
  return (
    <section className='grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-x-8 gap-y-12 mt-4'>
        {hotels.map(hotel => (
            <div key={hotel.id}>
                <HotelCard hotel={hotel} />
            </div>
        ))}
    </section>
  )
}