'use client'

import { useRouter } from 'next/navigation'
import { BookOpenCheck, ChevronsUpDown, HotelIcon, Plus } from 'lucide-react'

import { Button } from '@/components/ui/button'
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from '@/components/ui/dropdown-menu'

export function NavMenu() {
  const router = useRouter()

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant='ghost' className='border-none' size='icon'>
          <ChevronsUpDown className='size-5' />
          <span className='sr-only'>Nav menu</span>
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align='end'>
        <DropdownMenuItem className='cursor-pointer flex items-center gap-3' onClick={() => router.push('/hotel/new')}>
          <Plus className='size-5' /> <span>Add Hotel</span>
        </DropdownMenuItem>
        <DropdownMenuItem className='cursor-pointer flex items-center gap-3' onClick={() => router.push('/my-hotels')}>
          <HotelIcon className='size-5' /> <span>My Hotels</span>
        </DropdownMenuItem>
        <DropdownMenuItem className='cursor-pointer flex items-center gap-3' onClick={() => router.push('/my-bookings')}>
          <BookOpenCheck className='size-5' /> <span>My Bookings</span>
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  )
}
