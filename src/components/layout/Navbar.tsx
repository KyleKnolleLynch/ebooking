'use client'

import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/navigation'
import { UserButton, useAuth } from '@clerk/nextjs'

import Container from './Container'
import { Button } from '../ui/button'
import Searchbar from '../Searchbar'
import { ThemeToggle } from '../ThemeToggle'
import { NavMenu } from './NavMenu'

export default function Navbar() {
  const router = useRouter()
  const { userId } = useAuth()

  return (
    <div className='sticky top-0 border border-b-primary bg-secondary'>
      <Container>
        <div className='flex items-center justify-between gap-2'>
          <Link href='/' className='flex items-center gap-2'>
            <Image src='/logo.svg' alt='logo' width='30' height='30' priority />
            <h2 className='text-xl font-bold md:text-2xl'>Ebooking</h2>
          </Link>
          <Searchbar />
          <div className='flex items-center gap-1 md:gap-3'>
            <ThemeToggle />
            <NavMenu />
            <UserButton afterSignOutUrl='/' />
            {!userId && (
              <>
                <Button
                  onClick={() => router.push('/sign-in')}
                  variant='outline'
                >
                  Sign in
                </Button>
                <Button onClick={() => router.push('/sign-up')}>Sign up</Button>
              </>
            )}
          </div>
        </div>
      </Container>
    </div>
  )
}
