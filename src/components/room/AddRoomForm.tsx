'use client'

import { useEffect, useState } from 'react'
import Image from 'next/image'
import { useRouter } from 'next/navigation'
import axios from 'axios'
import * as z from 'zod'
import { Hotel, Room } from '@prisma/client'
import { Form, useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { Loader2, PencilIcon, PencilLine, XCircle } from 'lucide-react'
import {
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '../ui/form'
import { Input } from '../ui/input'
import { Textarea } from '../ui/textarea'
import { Checkbox } from '../ui/checkbox'
import { Button } from '../ui/button'
import { UploadButton } from '@/utils/uploadthing'
import { useToast } from '../ui/use-toast'

interface AddRoomFormProps {
  hotel?: Hotel & {
    rooms: Room[]
  }
  room?: Room
  handleDialogOpen: () => void
}

const formSchema = z.object({
  title: z.string().min(3, {
    message: 'Title must be at lease 3 characters long.',
  }),
  description: z.string().min(0, {
    message: 'Description must be at least 10 characters long.',
  }),
  bedCount: z.coerce.number().min(1, { message: 'Bed count is required' }),
  guestCount: z.coerce.number().min(1, { message: 'Guest count is required' }),
  bathroomCount: z.coerce
    .number()
    .min(1, { message: 'Bathroom count is required' }),
  sofaBedCount: z.coerce.number().min(0),
  murphyBedCount: z.coerce.number().min(0),
  image: z.string().min(1, {
    message: 'Image is required',
  }),
  breakfastPrice: z.coerce.number().optional(),
  roomPrice: z.coerce.number().min(1, {
    message: 'Room price is required',
  }),
  roomService: z.boolean().optional(),
  tv: z.boolean().optional(),
  kitchen: z.boolean().optional(),
  balcony: z.boolean().optional(),
  airConditioning: z.boolean().optional(),
  mediaStation: z.boolean().optional(),
  scenicView: z.boolean().optional(),
})

export default function AddRoomForm({
  hotel,
  room,
  handleDialogOpen,
}: AddRoomFormProps) {
  const router = useRouter()
  const [image, setImage] = useState<string | undefined>(room?.image)
  const [imageIsDeleting, setImageIsDeleting] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const { toast } = useToast()

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: room || {
      title: '',
      description: '',
      bedCount: 0,
      guestCount: 0,
      bathroomCount: 0,
      sofaBedCount: 0,
      murphyBedCount: 0,
      image: '',
      breakfastPrice: 0,
      roomPrice: 0,
      roomService: false,
      tv: false,
      kitchen: false,
      balcony: false,
      airConditioning: false,
      mediaStation: false,
      scenicView: false,
    },
  })

  useEffect(() => {
    if (typeof image === 'string') {
      form.setValue('image', image, {
        shouldValidate: true,
        shouldDirty: true,
        shouldTouch: true,
      })
    }
  }, [form, image])

  async function handleImageDelete(image: string) {
    setImageIsDeleting(true)
    const imageKey = image.substring(image.lastIndexOf('/') + 1)

    try {
      const res = await axios.post('/api/uploadthing/delete', { imageKey })
      if (res.data.success) {
        setImage('')
        toast({
          variant: 'success',
          description: 'Image removed!',
        })
      }
    } catch (error) {
      toast({
        variant: 'destructive',
        description: 'Something went wrong',
      })
    } finally {
      setImageIsDeleting(false)
    }
  }

  function onSubmit(values: z.infer<typeof formSchema>) {
    setIsLoading(true)
    if (hotel && room) {
      axios
        .patch(`/api/room/${room.id}`, values)
        .then(res => {
          toast({
            variant: 'success',
            description: '🚀 Room updated!',
          })
          setIsLoading(false)
          router.refresh()
          handleDialogOpen()
        })
        .catch(error => {
          console.log(error)
          toast({
            variant: 'destructive',
            description: 'Something went wrong!',
          })
          setIsLoading(false)
        })
    } else {
      axios
        .post('/api/room', { ...values, hotelId: hotel?.id })
        .then(res => {
          toast({
            variant: 'success',
            description: '🎆 Room created!',
          })
          setIsLoading(false)
          router.refresh()
          handleDialogOpen()
        })
        .catch(error => {
          console.log(error)
          toast({
            variant: 'destructive',
            description: 'Something went wrong!',
          })
          setIsLoading(false)
        })
    }
  }

  return (
    <div className='max-h-[75vh] overflow-y-auto px-2'>
      <Form {...form}>
        <form className='space-y-6'>
          <FormField
            control={form.control}
            name='title'
            render={({ field }) => (
              <FormItem>
                <FormLabel>Room Title</FormLabel>
                <FormDescription>Please provide a room name.</FormDescription>
                <FormControl>
                  <Input
                    placeholder='Single room'
                    {...field}
                    className='dark:border dark:border-muted-foreground'
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name='description'
            render={({ field }) => (
              <FormItem>
                <FormLabel>Room Description</FormLabel>
                <FormDescription>
                  Please list additional requirements for your room.
                </FormDescription>
                <FormControl>
                  <Textarea
                    placeholder='Ex. non-smoking or pet friendly'
                    {...field}
                    className='dark:border dark:border-muted-foreground'
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <div>
            <FormLabel>Choose room amenities</FormLabel>
            <FormDescription>Choose below to fit your needs.</FormDescription>
            <div className='mb-2 mt-2 grid grid-cols-2 gap-2'>
              <FormField
                control={form.control}
                name='roomService'
                render={({ field }) => (
                  <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                    <FormControl>
                      <Checkbox
                        id='roomService'
                        checked={field.value}
                        onCheckedChange={field.onChange}
                      />
                    </FormControl>
                    <FormLabel htmlFor='roomService'>Room service</FormLabel>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='tv'
                render={({ field }) => (
                  <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                    <FormControl>
                      <Checkbox
                        id='tv'
                        checked={field.value}
                        onCheckedChange={field.onChange}
                      />
                    </FormControl>
                    <FormLabel htmlFor='tv'>Television</FormLabel>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='kitchen'
                render={({ field }) => (
                  <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                    <FormControl>
                      <Checkbox
                        id='kitchen'
                        checked={field.value}
                        onCheckedChange={field.onChange}
                      />
                    </FormControl>
                    <FormLabel htmlFor='kitchen'>Kitchen</FormLabel>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='balcony'
                render={({ field }) => (
                  <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                    <FormControl>
                      <Checkbox
                        id='balcony'
                        checked={field.value}
                        onCheckedChange={field.onChange}
                      />
                    </FormControl>
                    <FormLabel htmlFor='balcony'>Balcony</FormLabel>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='airConditioning'
                render={({ field }) => (
                  <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                    <FormControl>
                      <Checkbox
                        id='airConditioning'
                        checked={field.value}
                        onCheckedChange={field.onChange}
                      />
                    </FormControl>
                    <FormLabel htmlFor='airConditioning'>
                      Air conditioning
                    </FormLabel>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='mediaStation'
                render={({ field }) => (
                  <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                    <FormControl>
                      <Checkbox
                        id='mediaStation'
                        checked={field.value}
                        onCheckedChange={field.onChange}
                      />
                    </FormControl>
                    <FormLabel htmlFor='mediaStation'>
                      Multi-media station
                    </FormLabel>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='scenicView'
                render={({ field }) => (
                  <FormItem className='flex items-end space-x-3 rounded-md border p-4 dark:border-muted-foreground'>
                    <FormControl>
                      <Checkbox
                        id='scenicView'
                        checked={field.value}
                        onCheckedChange={field.onChange}
                      />
                    </FormControl>
                    <FormLabel htmlFor='scenicView'>Scenic View</FormLabel>

                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
          </div>
          <FormField
            control={form.control}
            name='image'
            render={({ field }) => (
              <FormItem className='my-2 flex flex-col space-y-3'>
                <FormLabel>Upload an image</FormLabel>
                <FormDescription>
                  Choose an image that will showcase your room
                </FormDescription>
                <FormControl>
                  {image ? (
                    <>
                      <div className='relative mt-4 max-h-[400px] min-h-[200px] min-w-[200px] max-w-[400px]'>
                        <Image
                          fill
                          sizes='(max-width: 768px) 200px, 400px'
                          src={image}
                          alt='Room image'
                          className='object-contain'
                        />
                        <Button
                          type='button'
                          aria-label='Delete room image'
                          size='icon'
                          variant='ghost'
                          className='absolute -right-3 top-0'
                          onClick={() => handleImageDelete(image)}
                        >
                          {imageIsDeleting ? (
                            <Loader2 className='animate-spin' />
                          ) : (
                            <XCircle />
                          )}
                        </Button>
                      </div>
                    </>
                  ) : (
                    <>
                      <div className='flex max-w-[400px] flex-col items-center rounded-md border-2 border-dashed p-10 dark:border-muted-foreground/50'>
                        <UploadButton
                          endpoint='imageUploader'
                          onClientUploadComplete={res => {
                            setImage(res[0].url)
                            toast({
                              variant: 'success',
                              description: 'Upload successful!',
                            })
                            console.log('Files: ', res)
                          }}
                          onUploadError={(error: Error) => {
                            toast({
                              variant: 'destructive',
                              description: `Upload failed! ${error.message}`,
                            })
                          }}
                        />
                      </div>
                    </>
                  )}
                </FormControl>
              </FormItem>
            )}
          />
          <div className='flex gap-6'>
            <div className='flex flex-1 flex-col gap-6'>
              <FormField
                control={form.control}
                name='roomPrice'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>
                      Room Price <span className='ml-2 text-xs'>~ in USD</span>{' '}
                    </FormLabel>
                    <FormDescription>
                      Price per one full day/24 hours.
                    </FormDescription>
                    <FormControl>
                      <Input
                        type='number'
                        min={0}
                        {...field}
                        className='dark:border dark:border-muted-foreground'
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='bedCount'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Bed count</FormLabel>
                    <FormDescription>
                      Number of beds available in room.
                    </FormDescription>
                    <FormControl>
                      <Input
                        type='number'
                        min={0}
                        max={8}
                        {...field}
                        className='dark:border dark:border-muted-foreground'
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='guestCount'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Guest count</FormLabel>
                    <FormDescription>
                      Number of guest allowed in room.
                    </FormDescription>
                    <FormControl>
                      <Input
                        type='number'
                        min={0}
                        max={12}
                        {...field}
                        className='dark:border dark:border-muted-foreground'
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='bathroomCount'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Bathroom count</FormLabel>
                    <FormDescription>
                      Number of bathrooms in room.
                    </FormDescription>
                    <FormControl>
                      <Input
                        type='number'
                        min={1}
                        {...field}
                        className='dark:border dark:border-muted-foreground'
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
            <div className='flex flex-1 flex-col gap-6'>
              <FormField
                control={form.control}
                name='breakfastPrice'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>
                      Breakfast Price{' '}
                      <span className='ml-2 text-xs'>~ in USD</span>{' '}
                    </FormLabel>
                    <FormDescription>Price for full breakfast.</FormDescription>
                    <FormControl>
                      <Input
                        type='number'
                        min={0}
                        {...field}
                        className='dark:border dark:border-muted-foreground'
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='sofaBedCount'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Sofa-bed count</FormLabel>
                    <FormDescription>
                      Number of sofa-beds in room.
                    </FormDescription>
                    <FormControl>
                      <Input
                        type='number'
                        min={0}
                        {...field}
                        className='dark:border dark:border-muted-foreground'
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name='murphyBedCount'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Murphy bed count</FormLabel>
                    <FormDescription>
                      Number of Murphy beds in room.
                    </FormDescription>
                    <FormControl>
                      <Input
                        type='number'
                        min={0}
                        {...field}
                        className='dark:border dark:border-muted-foreground'
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
          </div>
          <div className='mb-2 mt-4'>
            {room ? (
              <Button
                type='button'
                disabled={isLoading}
                onClick={form.handleSubmit(onSubmit)}
                className='max-w-36'
              >
                {isLoading ? (
                  <>
                    <Loader2 className='mr-2 size-4 animate-spin' />
                    Updating
                  </>
                ) : (
                  <>
                    <PencilLine className='mr-2 size-4' />
                    Update
                  </>
                )}
              </Button>
            ) : (
              <Button
                type='button'
                onClick={form.handleSubmit(onSubmit)}
                className='max-w-36'
              >
                {isLoading ? (
                  <>
                    <Loader2 className='mr-2 size-4 animate-spin' />
                    Creating
                  </>
                ) : (
                  <>
                    <PencilIcon className='mr-2 size-4' />
                    Create room
                  </>
                )}
              </Button>
            )}
          </div>
        </form>
      </Form>
    </div>
  )
}
