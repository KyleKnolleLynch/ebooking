import { useEffect, useState } from 'react'
import { usePathname, useRouter } from 'next/navigation'
import Image from 'next/image'
import axios from 'axios'
import { DateRange } from 'react-day-picker'
import { Booking, Hotel, Room } from '@prisma/client'
import {
  Bath,
  Bed,
  BedDouble,
  BookImage,
  IceCream2,
  Loader2,
  MonitorSmartphone,
  PencilLine,
  Sofa,
  Theater,
  Trash2,
  Tv,
  UsersRound,
  Utensils,
  Wind,
} from 'lucide-react'

import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from '../ui/card'
import AmenityItem from '../AmenityItem'
import { Separator } from '../ui/separator'
import { Button } from '../ui/button'
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '../ui/dialog'
import AddRoomForm from './AddRoomForm'

import { useToast } from '../ui/use-toast'
import { DatePickerWithRange } from './DateRangePicker'
import { differenceInCalendarDays } from 'date-fns'
import { Checkbox } from '../ui/checkbox'

interface RoomCardProps {
  hotel?: Hotel & {
    rooms: Room[]
  }
  room: Room
  bookings?: Booking[]
}

export default function RoomCard({
  hotel,
  room,
  bookings = [],
}: RoomCardProps) {
  const [isLoading, setIsLoading] = useState(false)
  const [open, setOpen] = useState(false)
  const [date, setDate] = useState<DateRange | undefined>()
  const [totalPrice, setTotalPrice] = useState(room.roomPrice)
  const [includesBreakfast, setIncludesBreakfast] = useState(false)
  const [days, setDays] = useState(0)

  const pathname = usePathname()
  const router = useRouter()
  const { toast } = useToast()
  const isHotelDetailsPage = pathname.includes('hotel-details')

  useEffect(() => {
    if (date && date.from && date.to) {
      const dayCount = differenceInCalendarDays(date.to, date.from)

      setDays(dayCount)

      if (dayCount && room.roomPrice) {
        if (includesBreakfast && room.breakfastPrice) {
          setTotalPrice(
            dayCount * room.roomPrice * (dayCount * room.breakfastPrice),
          )
        } else {
          setTotalPrice(dayCount * room.roomPrice)
        }
      } else {
        setTotalPrice(room.roomPrice)
      }
    }
  }, [date, room.roomPrice, includesBreakfast])

  const handleDialogOpen = (): void => {
    setOpen(prev => !prev)
  }

  const handleRoomDelete = async (room: Room) => {
    setIsLoading(true)
    const imageKey = room.image.substring(room.image.lastIndexOf('/') + 1)
    try {
      await axios.post('/api/uploadthing/delete', { imageKey })

      await axios.delete(`/api/room/${room.id}`)

      setIsLoading(false)
      toast({
        variant: 'success',
        description: 'Room deleted!',
      })
      router.refresh()
    } catch (error: any) {
      setIsLoading(false)
      toast({
        variant: 'destructive',
        description: `Something went wrong! Error: ${error.message}`,
      })
    }
  }

  return (
    <Card>
      <CardHeader>
        <CardTitle>{room.title}</CardTitle>
        <CardDescription>{room.description}</CardDescription>
      </CardHeader>
      <CardContent className='flex flex-col gap-4'>
        <div className='relative aspect-square h-48 overflow-hidden rounded-lg'>
          <Image
            src={room.image}
            alt={room.title}
            fill
            className='object-cover'
          />
        </div>
        <ul className='grid grid-cols-2 content-start gap-4 text-sm'>
          <AmenityItem>
            <UsersRound className='size-5' />
            {room.guestCount} Guest{room.guestCount > 1 && 's'}
          </AmenityItem>
          <AmenityItem>
            <BedDouble className='size-5' />
            {room.bedCount} Bed{room.bedCount > 1 && 's'}
          </AmenityItem>
          <AmenityItem>
            <Bath className='size-5' />
            {room.bathroomCount} Bathroom{room.bathroomCount > 1 && 's'}
          </AmenityItem>
          {!!room.sofaBedCount && (
            <AmenityItem>
              <Sofa className='size-5' />
              {room.sofaBedCount} Sofa-bed{room.sofaBedCount > 1 && 's'}
            </AmenityItem>
          )}
          {!!room.murphyBedCount && (
            <AmenityItem>
              <Bed className='size-5' />
              {room.murphyBedCount} Murphy Bed{room.murphyBedCount > 1 && 's'}
            </AmenityItem>
          )}
          {room.roomService && (
            <AmenityItem>
              <IceCream2 className='size-5' />
              Room Service
            </AmenityItem>
          )}
          {room.tv && (
            <AmenityItem>
              <Tv className='size-5' />
              Television
            </AmenityItem>
          )}
          {room.kitchen && (
            <AmenityItem>
              <Utensils className='size-5' />
              Kitchen
            </AmenityItem>
          )}
          {room.balcony && (
            <AmenityItem>
              <Theater className='size-5' />
              Balcony
            </AmenityItem>
          )}
          {room.airConditioning && (
            <AmenityItem>
              <Wind className='size-5' />
              Air Conditioning
            </AmenityItem>
          )}
          {room.mediaStation && (
            <AmenityItem>
              <MonitorSmartphone className='size-6' />
              Multi-Media Station
            </AmenityItem>
          )}
          {room.scenicView && (
            <AmenityItem>
              <BookImage className='size-5' />
              Scenic View
            </AmenityItem>
          )}
        </ul>
        <Separator className='bg-foreground/30' />
        <ul className='flex justify-between gap-4'>
          <li>
            Room Price: <span className='font-bold'>{room.roomPrice}</span>
            <span className='text-xs'>/24hrs</span>
          </li>
          {!!room.breakfastPrice && (
            <li>
              Breakfast Price:{' '}
              <span className='font-bold'>{room.breakfastPrice}</span>
            </li>
          )}
        </ul>
      </CardContent>
      <CardFooter>
        {isHotelDetailsPage ? (
          <div className='flex flex-col gap-6'>
            <div>
              <p className='mb-2'>
                Select the number of days you wish to occupy room.
              </p>
              <DatePickerWithRange date={date} setDate={setDate} />
            </div>
            {room.breakfastPrice > 0 && (
              <div>
                <p>Would you like breakfast each day?</p>
                <div className='flex items-center space-x-2'>
                  <Checkbox
                    id='breakfast'
                    onCheckedChange={val => setIncludesBreakfast(!!val)}
                  />
                  <label htmlFor="breakfast">Include breakfast</label>
                </div>
              </div>
            )}
            <div>
              Total Price: <span className='mr-2 font-bold'>${totalPrice}</span>
              for<span className='ml-2 font-bold'>{days} days</span>
            </div>
          </div>
        ) : (
          <div className='flex w-full justify-between'>
            <Button
              disabled={isLoading}
              type='button'
              variant='ghost'
              onClick={() => handleRoomDelete(room)}
            >
              {isLoading ? (
                <>
                  <Loader2 className='mr-2 size-4 animate-spin' /> Deleting...
                </>
              ) : (
                <>
                  <Trash2 className='mr-2 size-4' /> Delete
                </>
              )}
            </Button>
            <Dialog open={open} onOpenChange={setOpen}>
              <DialogTrigger>
                <Button type='button' variant='outline' className='max-w-36'>
                  <PencilLine className='mr-2 size-4' />
                  Update room
                </Button>
              </DialogTrigger>
              <DialogContent className='w-11/12 max-w-[900px]'>
                <DialogHeader>
                  <DialogTitle>Update room</DialogTitle>
                  <DialogDescription>
                    Make some changes to your room.
                  </DialogDescription>
                </DialogHeader>
                <AddRoomForm
                  hotel={hotel}
                  handleDialogOpen={handleDialogOpen}
                  room={room}
                />
              </DialogContent>
            </Dialog>
          </div>
        )}
      </CardFooter>
    </Card>
  )
}
